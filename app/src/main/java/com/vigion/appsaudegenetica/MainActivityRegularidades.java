package com.vigion.appsaudegenetica;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivityRegularidades extends AppCompatActivity implements View.OnClickListener{

    Button orelhas, olhos, labios, cabelo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_regularidades);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        orelhas = (Button) findViewById(R.id.btnOrelhas);
        olhos = (Button) findViewById(R.id.btnOlhos);
        labios = (Button) findViewById(R.id.btnLabios);
        cabelo = (Button) findViewById(R.id.btnCabelo);

        orelhas.setOnClickListener(this);
        olhos.setOnClickListener(this);
        labios.setOnClickListener(this);
        cabelo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnOrelhas:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnOlhos:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnLabios:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnCabelo:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
        }
    }
}