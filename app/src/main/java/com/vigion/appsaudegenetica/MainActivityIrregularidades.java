package com.vigion.appsaudegenetica;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivityIrregularidades extends AppCompatActivity implements View.OnClickListener{

    Button sardas, diabetes, colestrol, cancro;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_irregularidades);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        sardas = (Button) findViewById(R.id.btnSardas);
        diabetes = (Button) findViewById(R.id.btnDiabetes);
        colestrol = (Button) findViewById(R.id.btnColestrol);
        cancro = (Button) findViewById(R.id.btnCancro);

        sardas.setOnClickListener(this);
        diabetes.setOnClickListener(this);
        colestrol.setOnClickListener(this);
        cancro.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {

        switch (v.getId()){
            case R.id.btnSardas:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnDiabetes:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnColestrol:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
            case R.id.btnCancro:
                startActivity(new Intent(this, MainActivityArvoreGeneologica.class));
                break;
        }
    }
}