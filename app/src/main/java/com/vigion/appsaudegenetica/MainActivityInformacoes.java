package com.vigion.appsaudegenetica;

import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;

public class MainActivityInformacoes extends AppCompatActivity implements View.OnClickListener{

    Button hereditariedade, gene, alelo, genotipo, fenotipo, dominante, recessivo;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main_activity_informacoes);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });

        hereditariedade = (Button) findViewById(R.id.btnHereditariedade);
        gene = (Button) findViewById(R.id.btnGene);
        alelo = (Button) findViewById(R.id.btnAlelo);
        genotipo = (Button) findViewById(R.id.btnGenotipo);
        fenotipo = (Button) findViewById(R.id.btnFenotipo);
        dominante = (Button) findViewById(R.id.btnDominante);
        recessivo = (Button) findViewById(R.id.btnRecessivo);

        hereditariedade.setOnClickListener(this);
        gene.setOnClickListener(this);
        alelo.setOnClickListener(this);
        genotipo.setOnClickListener(this);
        fenotipo.setOnClickListener(this);
        dominante.setOnClickListener(this);
        recessivo.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.btnHereditariedade:
                startActivity(new Intent(this, MainActivityHereditariedade.class));
                break;
            case R.id.btnGene:
                startActivity(new Intent(this, MainActivityGene.class));
                break;
            case R.id.btnAlelo:
                startActivity(new Intent(this, MainActivityAlelo.class));
                break;
            case R.id.btnGenotipo:
                startActivity(new Intent(this, MainActivityGenotipo.class));
                break;
            case R.id.btnFenotipo:
                startActivity(new Intent(this, MainActivityFenotipo.class));
                break;
            case R.id.btnDominante:
                startActivity(new Intent(this, MainActivityDominante.class));
                break;
            case R.id.btnRecessivo:
                startActivity(new Intent(this, MainActivityRecessivo.class));
                break;
        }
    }
}
